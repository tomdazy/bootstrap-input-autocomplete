/*! Copyright 2022 Thomas Dazy thomasdazy.fr */

/**
 *  jQuery input autocomplete
 *
 *  Librarie permettant d'avoir un menu d'autocompletion sur un
 *  champ de saisie texte
 *
 **/

(function ($) {
  $.fn.inputAutocomplete = function(options) {

    // Options de la librairie
    var settings = $.extend({
      minLength: 2,
      urlSource: 'input.php'
    }, options );


    // Donne l'ID random de l'input
    function getAutocompleteID(input) {
      var attr = input.attr('input_autocomplete_id');
      if (typeof attr !== typeof undefined && attr !== false) {
        return input.attr('input_autocomplete_id');
      }
      else {
        var autocomplete_id = Math.floor(Math.random() * 100000000) + 1
        input.attr('input_autocomplete_id', autocomplete_id);
        return autocomplete_id;
      }
    }

    // Donne l'ID du dropdown
    function getDropdownID(input) {
      var dropdown_id = 'dropdown_input_autocomplete_' + getAutocompleteID(input);
      return dropdown_id;
    }

    // Fonction qui crée le dropdown
    function createDropdown(input) {
      var dropdown = `<div class="dropdown-menu input_autocomplete_dropdown" id="${getDropdownID(input)}"></div>`;

      input.addClass('dropdown-toggle');
      input.attr('data-toggle', 'dropdown');
      input.attr('aria-haspopup', 'true');
      input.attr('aria-expanded', 'true');
      input.after(dropdown);
    }

    // Vérifie si le dropdown existe
    function doesDropdownExists(input) {
      if ($('#' + getDropdownID(input)).length) return true;
      else return false;
    }

    // Supprime le dropdown
    function removeDropdown(input) {
      $('#' + getDropdownID(input)).remove();
    }

    // Fonction qui vide le dropdown
    function emptyDropdown(dropdown) {
      dropdown.find('.dropdown-item').remove();
      dropdown.find('.dropdown-divider').remove();
    }

    // Fonction qui remplit le dropdown avec les nouveaux éléments
    function populateDropdown(dropdown, datas) {
      // On ajoute les nouveaux
      $.each(datas, function(i, item) {
        // Si on veut ajouter un séparateur
        if (typeof item.type !== typeof undefined && item.type !== false && item.type == 'separator') {
          dropdown.append('<div role="separator" class="dropdown-divider"></div>');
        }
        // Si on veut ajouter une valeur sélectionable
        else {
          if (typeof item.value !== typeof undefined && item.value !== false) var value = item.value;
          else var value = item.text;
          // On ajoute la valeur dans le select
          dropdown.append(`<a class="dropdown-item" data-value="${value}" href="#">${item.text}</a>`);
        }
      });
    }

    // Fonction qui active la vérification de si un élément du dropdown à été sélectionnée
    function enableSelectEvent(dropdown, input) {
      // On gère l'event de sélection dans le dropdown
      dropdown.find('.dropdown-item').click(function(){
        var attr = $(this).attr('data-value');
        if (typeof attr !== typeof undefined && attr !== false) var value = attr; // Si l'élément à un attribut value
        else var value = $(this).html();  // Sinon la value est le texte qu'il contient

        // On remplace la value de l'input pas la nouvelle valeur sélectionnée
        input.val(value)
        // On trigger les évents liés à la modification du champ pour les autres scripts
        input.trigger('keyup').trigger('keypress').trigger('keydown');
      });
    }


    var input = this;

    // On récupère l'ID random de l'input
    autocomplete_id = getAutocompleteID(input);
    // On récupère l'ID du dropdown
    dropdown_id = getDropdownID(input);
    // On supprime le dropdown s'il existe déjà
    if (doesDropdownExists(input)) $('#' + dropdown_id).remove();
    // On crée le dropdown
    createDropdown(input);
    // On stocke le dropdown dans une variable pour plus tard
    var dropdown_element = $('#' + dropdown_id);

    // Quand l'utilisateur écrit dans le champ de saisie
    input.keyup(function(){
      // Si la valeur saisie est plus longue que le minimum requis
      if ((input.val().length >= settings.minLength )) {
        // On récupère les données
        $.ajax({
          url : settings.urlSource,
          type : 'POST',
          data : 'search=' + input.val(),
          dataType : 'json',
          success : function(code_html, statut) {
            // On retire les éléments précédents du select
            emptyDropdown(dropdown_element);

            if (code_html.length) {
              // Remplis le dropdown
              populateDropdown(dropdown_element, code_html)
              dropdown_element.dropdown();
            }

            // Active l'event si on sélectionne une option du dropdown
            enableSelectEvent(dropdown_element, input);
          }
        });
      }
      // Si on a pas le nombre minimum de caractères pour l'autocomlétion alors on vide le dropdown
      else emptyDropdown(dropdown_element);
    });
    return true;
  };
}( jQuery ));
