# Bootstrap Input Autocomplete

<!-- TOC -->
- [Description](#description)
- [Usage](#usage)
  - [Result](#result)
  - [Code](#code)
    - [`index.html`](#indexhtml)
    - [`source.json`](#sourcejson)
- [Installation](#installation)
  - [Manual installation](#manual-installation)
  - [Automatic installation (NPM)](#automatic-installation-npm)
- [License](#license)
- [What's next, about me](#whats-next-about-me)
<!-- /TOC -->

## Description
Autocomplete on a text input field with Bootstrap dropdown and Ajax data source

## Usage
### Result
![Capture](https://gitlab.com/tomdazy/bootstrap-input-autocomplete/-/raw/c558c4ba4b98341e728ecab6302671bd7cf515b2/assets/README-2c4371e9.png)

### Code
#### `index.html`
The example below don't have any styling to it
```html
<!-- Input field -->
<input type="text" class="inputAutocomplete">

<script>
  $(document).ready(function(){
    $('.inputAutocomplete').inputAutocomplete({
      urlSource: 'source.json',  // Source file for dropdown values
      minLength: 1  // Minimum input length for autocompletion to show
    });
  });
</script>
```

#### `source.json`
POST: `search=o` (the input field have the value 'o')
```json
[
  {
    "text": "Volvo",
    "value": "vlvo"
  },
  {"text": "Opel"},
  {"text": "Peugeot"},
  {"type": "separator"},
  {
    "text": "Tesla Motors",
    "value": "tsla"
  }
]
```

In each of the source json array value we have multiple elements:

Name | Values | Description
--|---|--
 `text` | `string` | Text shown in the dropdown
 `value` | `string` | (Optional) Value put in input field when clicking on the option
 `type` | `string` | Set to `separator` if you want a separating line

## Installation
### Manual installation
You need to download the sources and include them like this:
```html
<!-- Bootstrap Input Autocomplete script -->
<script src="jquery.input_autocomplete.js"></script>
```
Some dependencies are required:
- **jQuery 3.x** (not the slim, it doesn't include Ajax which is required)
- **Bootstrap 4.x**

### Automatic installation (NPM)
Install with NPM:
`npm i bootstrap-input-autocomplete`

## License
You are free to use this library for any project as long as my name is somewhere in the credits.

## What's next, about me
I'm beginning in my coding life, I have a lot of project and ideas, if you liked my work feel free to leave a tip:


[![Paypal](https://gitlab.com/tomdazy/bootstrap-input-autocomplete/-/raw/main/assets/README-92c54db5.png)](https://www.paypal.com/donate/?hosted_button_id=EUTTJLXH8TWBC)

[![Buy me a Coffee](https://gitlab.com/tomdazy/bootstrap-input-autocomplete/-/raw/main/assets/README-ad627c8e.png)](https://www.buymeacoffee.com/ThomasDazy)
